const { exec, spawn } = require('child_process')
const Promise = require('bluebird')
const WebSocket = require('ws')
const wss = new WebSocket.Server({ port: 7123 })

const processTimeout = 2000
const interval = 5000
const interfaces = {
  WiFi: 'wlan0',
  AP: 'eth0',
  Modem: 'eth1',
  VPN: 'tun0',
}
const checkIPs = {
  WiFi: '192.168.12.1',
  AP: '192.168.1.129',
  Modem: '192.168.1.1',
  VPN: '10.8.0.1',
}

const ifaceRateData = {}
let cpuUsage

function handleAction(action) {
  if (action === 'Stop VPN') exec('systemctl stop openvpn-client@shervin-vps')
  else if (action === 'Restart VPN') exec('systemctl restart openvpn-client@shervin-vps')
  else if (action === 'Restart Router') exec('reboot')
  else if (action === 'Restart Modem') exec('./restart-modem.sh')
}

function initWebSocket() {
  wss.on('connection', (ws) => {
    ws.on('message', handleAction)
    sendAllData()
  })
}

function formatByte(value) {
  if (value > 1024 * 1024) return Math.floor(value / 1024 / 1024) + 'GiB'
  else if (value > 1024) return Math.floor(value / 1024) + 'MiB'
  else return value + 'B'
}

function getInterfaceUsedData() {
  const ifaceDatas = {}
  const promises = []
  Object.keys(interfaces).forEach((name) => {
    const iface = interfaces[name]
    ifaceDatas[name]= { Today: { Up: 'None', Down: 'None' }, Month: { Up: 'None', Down: 'None' } }
    promises.push(new Promise((resolve, reject) => {
      let timeoutId = null
      const proc = exec(`vnstat -i ${iface} -q -s --json`, (err, stdout, stderr) => {
        clearTimeout(timeoutId)
        try {
          const data = JSON.parse(stdout)
          const dayUp = data.interfaces[0].traffic.days[0] && formatByte(data.interfaces[0].traffic.days[0].tx)
          const dayDown = data.interfaces[0].traffic.days[0] && formatByte(data.interfaces[0].traffic.days[0].rx)
          const monthUp = data.interfaces[0].traffic.months[0] && formatByte(data.interfaces[0].traffic.months[0].tx)
          const monthDown = data.interfaces[0].traffic.months[0] && formatByte(data.interfaces[0].traffic.months[0].rx)
          ifaceDatas[name]= { Today: { Up: dayUp, Down: dayDown }, Month: { Up: monthUp, Down: monthDown } }
        } catch (e) {}
        resolve()
      })
      timeoutId = setTimeout(() => proc.kill(), processTimeout)
    }))
  })
  return new Promise((resolve, reject) => {
    Promise.all(promises).then(() => resolve(ifaceDatas))
  })
}

function parseBooleanValue(value) {
  return value ? 'OK' : 'FAIL'
}

function getInterfaceStatus() {
  const ifaceDatas = {}
  const promises = []
  Object.keys(interfaces).forEach((name) => {
    ifaceDatas[name] = { Status: parseBooleanValue(false) }
    promises.push(new Promise((resolve, reject) => {
      let timeoutId = null
      const proc = exec(`ping -c 1 ${checkIPs[name]}`, (err, stdout, stderr) => {
        clearTimeout(timeoutId)
        try {
          ifaceDatas[name].Status = parseBooleanValue(stdout.match(/(\d+) received/)[1] === '1')
        } catch (e) {}
        resolve()
      })
      timeoutId = setTimeout(() => proc.kill(), processTimeout)
    }))
  })
  return new Promise((resolve, reject) => {
    Promise.all(promises).then(() => resolve(ifaceDatas))
  })
}

function initInterfaceRate(ifaceName) {
  const iface = interfaces[ifaceName]
  const proc = spawn('vnstat', ['-i', iface, '-l'])
  ifaceRateData[ifaceName] = { Traffic: { Up: 'None', Down: 'None' } }
  proc.stdout.on('data', (data) => {
    const str = data.toString('utf8')
    const strParts = str.split(' ').filter(str => str).slice(1)
    if (strParts[0] === 'rx:') {
      const Down = processRateValue(strParts[1], strParts[2])
      const Up = processRateValue(strParts[6], strParts[7])
      ifaceRateData[ifaceName] = { Traffic: { Up, Down } }
    }
  })
  proc.on('close', () => setTimeout(() => initInterfaceRate(ifaceName), 1000))
}

function processRateValue(value, suffix) {
  suffix = suffix.toLowerCase().charAt(0)
  return value + ' ' + suffix + 'bps'
}

function initInterfacesRate() {
  Object.keys(interfaces).forEach(initInterfaceRate)
}

function initCPU() {
  const proc = spawn('mpstat', [Math.floor(interval / 2000)])
  const createDataProcessor = () => {
    let counter = -1
    return (data) => {
      if (data === 'all') {
        counter = 0
        return false
      } else {
        counter += 1
        return counter === 10
      }
    }
  }
  const counterProcessor = createDataProcessor()
  proc.stdout.on('data', (data) => {
    const str = data.toString('utf8')
    const strParts = str.split(' ').filter(str => str)
    const results = strParts.filter(counterProcessor)
    if (results.length > 0) {
      const idleProccessor = parseFloat(results[results.length - 1])
      cpuUsage = (100 - idleProccessor).toFixed(2)
    }
  })
  proc.on('close', () => setTimeout(() => initCPU(), 1000))
}

function getMemory() {
  return new Promise((resolve, reject) => {
    let timeoutId = null
    const proc = exec('free', (err, stdout, stderr) => {
      clearTimeout(timeoutId)
      let memUsage
      try {
        const parts = stdout.split('\n')[1].split(' ').filter(str => str)
        const total = parseInt(parts[1])
        const avail = parseInt(parts[6])
        memUsage = (100 * (total - avail) / total).toFixed(2)
      } catch (e) {}
      resolve(memUsage)
    })
    timeoutId = setTimeout(() => proc.kill(), processTimeout)
  })
}

function getUptime() {
  return new Promise((resolve, reject) => {
    let timeoutId = null
    const proc = exec('awk \'{printf("%d:%02d:%02d",($1/60/60/24),($1/60/60%24),($1/60%60))}\' /proc/uptime', (err, stdout, stderr) => {
      clearTimeout(timeoutId)
      resolve(stdout)
    })
    timeoutId = setTimeout(() => proc.kill(), processTimeout)
  })
}

function getDNS(local) {
  return new Promise((resolve, reject) => {
    let timeoutId = null
    const proc = exec('nslookup google.com' + (local ? ' 127.0.0.1' : ''), (err, stdout, stderr) => {
      clearTimeout(timeoutId)
      let result = false
      try {
        result = !!stdout.match(/Name:\s*google.com\nAddress:/)
      } catch (e) {}
      resolve(parseBooleanValue(result))
    })
    timeoutId = setTimeout(() => proc.kill(), processTimeout)
  })
}

function getRoute(host) {
  return new Promise((resolve, reject) => {
    let timeoutId = null
    const proc = exec(`traceroute -m 1 ${host}`, (err, stdout, stderr) => {
      clearTimeout(timeoutId)
      let result = 'Fail'
      try {
        const gateway = stdout.split('\n')[1].match(/\((.*)\)/)[1]
        if (gateway === checkIPs.VPN) result = 'VPN'
        else result = 'Direct'
      } catch (e) {}
      resolve(result)
    })
    timeoutId = setTimeout(() => proc.kill(), processTimeout)
  })
}

function getPing(host) {
  return new Promise((resolve, reject) => {
    let timeoutId = null
    const proc = exec(`ping -c 1 ${host}`, (err, stdout, stderr) => {
      clearTimeout(timeoutId)
      let result = false
      try {
        result = stdout.match(/(\d+) received/)[1] === '1'
      } catch (e) {}
      resolve(parseBooleanValue(result))
    })
    timeoutId = setTimeout(() => proc.kill(), processTimeout)
  })
}

function init() {
  initInterfacesRate()
  initCPU()
  initWebSocket()
}

function mergeRecursive(obj1, obj2) {
  for (const p in obj2) {
    if (obj2.hasOwnProperty(p)) {
      if (p in obj1 && typeof obj2[p] === 'object') {
        obj1[p] = mergeRecursive(obj1[p], obj2[p])
      } else {
        obj1[p] = obj2[p]
      }
    }
  }
  return obj1
}

function processRoute(route, ping) {
  return ping === parseBooleanValue(false) ? ping : route
}

function getAllData() {
  const promises = [getInterfaceStatus(), getInterfaceUsedData(), getMemory(), getUptime(), getDNS(false),
    getDNS(true), getRoute('iran.ir'), getRoute('google.com'), getPing('iran.ir'), getPing('google.com')]
  return Promise.all(promises).then(([status, usedData, memory, uptime, globalDNS, localDNS, irRoute, globeRoute, irPing, globePing]) => {
    mergeRecursive(status, usedData)
    mergeRecursive(status, ifaceRateData)
    const systemData = { System: { CPU: cpuUsage + '%', Memory: memory + '%', Uptime: uptime } }
    const DNS = { DNS: { Global: globalDNS, Local: localDNS } }
    const Route = { Route: { IR: processRoute(irRoute, irPing), Globe: processRoute(globeRoute, globePing) } }
    return Object.assign(status, systemData, DNS, Route)
  })
}

function sendAllData() {
  getAllData().then((data) =>{
    wss.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify(data))
      }
    })
  })
}

init()
setInterval(() => sendAllData(), 5000)
